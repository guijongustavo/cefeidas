# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
set key autotitle columnhead
unset key
set output "./images/xsygni.png"
set title "AAVSO/OPTICAL X CYG 2039+35"
set xlabel "MJD"
set ylabel "MAGNITUDE"
set size 1,0.5

plot 'data/x_sygni_optical_clean.dat' using 1:2:3 title "Magnitude" with yerrorbars lc "blue"

