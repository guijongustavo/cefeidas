# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key
set title "Window Function Optical X CYG 2039+35"
set output "dft-wf-optical-xsygni.png"
set xlabel "Frequency [c/d]"
set ylabel "Window Function Power"

plot "stdin.dftclean.wfunc" u 1:($2*$2+$3*$3)
