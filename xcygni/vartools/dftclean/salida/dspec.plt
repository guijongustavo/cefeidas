# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key
set title "DFT Power Spectrum Optical X CYG 2039+35"
set output "dft-dspec-optical-xsygni.png"
set xlabel "Frequency [c/d]"
set ylabel "Power"


plot "stdin.dftclean.dspec" u 1:2 w l
