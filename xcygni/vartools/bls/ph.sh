#! /bin/bash
 # Made by:
 # Gustavo Magallanes-Guijon <gustavo.magallanes.guijon@ciencias.unam.mx>
 # Instituto de Astronomia UNAM
 # Ciudad Universitaria
 # Ciudad de Mexico
 # Mexico
 # Thu Jan  7 22:02:00 UTC 2016
 #
 # Last Modified: Fri Nov 11 16:38:04 UTC 2005

awk '{print $1" "$2" "$3}'  ../../lc/data/x_sygni_optical_clean.dat | \
	vartools -i - -oneline -parallel 4\
    -BLS q 0.01 0.1 1 100.0 1000 200 5 1 \
	0 0 0 \
    -Phase bls T0 bls 0.5 \
    -o salida/out.phase.txt \
    -binlc 1 nbins 200 0 \
    -o salida/out.phasebin.txt > ph
