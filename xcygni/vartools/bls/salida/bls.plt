# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key 
set title "BLS Periodogram of Optical X CYG 2039+35"
set output "bls-optical-xsygni.png"
set xlabel "Period [d]"
set ylabel "BLS SR"


plot "stdin.bls" u 1:2 w l
