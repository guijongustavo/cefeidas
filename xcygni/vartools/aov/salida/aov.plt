# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key 
set title "AoV Periodogram Optical X CYG 2039+35"
set output "aov-optical-xsygni.png"
set xlabel "Period [d]"
set ylabel "AOV"


plot "stdin.aov" u 1:2 w l
