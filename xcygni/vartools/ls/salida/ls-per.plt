# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key 
set output "ls-optical-xsygni.png"
set title "Lomb-Scargle Periodogram X CYG 2039+35"
set xlabel "Period [d]"
set ylabel "L-S Power"

plot "stdin.ls" u (1/$1):2 w l


