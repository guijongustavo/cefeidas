# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key 
set output "ls-optical-freq-ucarinae.png"
set title "Lomb-Scargle Periodogram X CYG 2039+35"
set xlabel "Frequency [c/d]"
set ylabel "L-S Power"
#set xrange[0.057:0.065]
set xrange[0.02:0.03]
set size  1,0.5
plot "stdin.ls" u 1:2 w l


