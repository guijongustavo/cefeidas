# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
set key 
set output "ph-optical-xsygni.png"
set title "Phased, and phase-binned light curves Optical X CYG 2039+35"
set xlabel "Phase"
set ylabel "Magnitude"

plot "out.phase.txt", \
"out.phasebin.txt"u 1:2 w l lt rgb "red" #, \
