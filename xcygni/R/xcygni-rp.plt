# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
set key autotitle columnhead
unset key
set key off
set output "images/xcyni-optico-rp.png"
set title "RobPer Periodogram X CYG 2039+35"
set xlabel "Trial Period (days)"
set ylabel "Coeficient of determination"
#set yrange[0:0.003]
plot "data/optical_rp_x_sygni-1_100.csv" with impulses 

