#! /bin/bash

# Made by:
# Gustavo Magallanes-Guijon <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico
# Thu Jan  7 22:02:00 UTC 2016
#
# Last Modified: Fri Nov 11 16:38:04 UTC 2005

python2.7 wwz.py -f ../carinae_optical_clean.dat -g -m -c=0.00001 -p=48 -o salida.txt -l=0.02 -hi=0.03 -d=0.00001 > wwz


