# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

#set terminal pngcairo size 640,480
set terminal png size 640,480
unset key 
set pm3d map

set title "WWZ of U CARINAE Optical"
set output "wwz-ucarinae.png"
set xlabel "MJD"
set ylabel "Frequency" 
splot "salida.txt" u 1:2:3 
