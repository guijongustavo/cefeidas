# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key 
set title "AoV Periodogram Optical U CARINAE 1053-59"
set output "aov-optical-ucarinae.png"
set xlabel "Period [d]"
set ylabel "AOV"


plot "stdin.aov_harm" u 1:2 w l
