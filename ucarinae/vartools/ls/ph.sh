#! /bin/bash
 # Made by:
 # Gustavo Magallanes-Guijon <gustavo.magallanes.guijon@ciencias.unam.mx>
 # Instituto de Astronomia UNAM
 # Ciudad Universitaria
 # Ciudad de Mexico
 # Mexico
 # Thu Jan  7 22:02:00 UTC 2016
 #
 # Last Modified: Fri Nov 11 16:38:04 UTC 2005


awk '{print $1" "$2" "$3}' ../../lc/data/carinae_optical_clean.dat | \
	vartools -i - -header -parallel 4 \
	-LS 1 100. 0.1 1 0 whiten clip 5. 1 \
	-Phase ls \
    -o salida/out.phase.txt \
	-binlc 1 nbins 200 0 \
    -o salida/out.phasebin.txt > ph
