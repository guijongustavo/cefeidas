#! /bin/bash
 # Made by:
 # Gustavo Magallanes-Guijon <gustavo.magallanes.guijon@ciencias.unam.mx>
 # Instituto de Astronomia UNAM
 # Ciudad Universitaria
 # Ciudad de Mexico
 # Mexico
 # Thu Jan  7 22:02:00 UTC 2016
 #
 # Last Modified: Fri Nov 11 16:38:04 UTC 2005


awk '{print $1" "$2" "$3}' ../../lc/data/carinae_optical_clean.dat | \
	vartools -i - -header -parallel 4\
	-aov Nbin 20 1 100. 0.1 0.01 5 0 \
	-Phase aov \
    -o salida/out.phase.txt \
	-binlc 1 nbins 200 0 \
    -o salida/out.phasebin.txt > ph
