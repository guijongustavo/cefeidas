awk '{print $1" "$2" "$3}' ../../lc/data/carinae_optical_clean.dat | \
	vartools -i - -oneline -ascii -parallel 4\
	-aov Nbin 20 1 100. 0.1 0.01 5 1 salida \
	whiten clip 5. 1 > aov
