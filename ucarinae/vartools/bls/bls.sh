awk '{print $1" "$2" "$3}' ../../lc/data/carinae_optical_clean.dat | \
	vartools -i - -ascii -oneline -parallel 4\
    -BLS q 0.01 0.1 1 100.0 1000 200 0 5 \
        1 salida/ 1 salida/ 0 fittrap \
        nobinnedrms ophcurve salida/ -0.1 1.1 0.001 > bls
