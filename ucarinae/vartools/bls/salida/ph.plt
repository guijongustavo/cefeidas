# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
set key 
set output "ph-optical-ucarinae.png"
set title "Phased, and phase-binned light curves U CARINAE 1053-59"
set xlabel "Phase"
set ylabel "Magnitude"
plot "out.phase.txt", "out.phasebin.txt"u 1:2  w l lt rgb "red" lw 2 
