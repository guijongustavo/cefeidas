# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key 
set title "BLS Periodogram of Optical U CARINAE 1053-59"
set output "bls-optical-ucarinae.png"
set xlabel "Period [d]"
set ylabel "BLS SR"


plot "stdin.bls" u 1:2 w l
