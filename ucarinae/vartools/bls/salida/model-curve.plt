# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
set key 
set output "model-curve-optical.png"
set title "Phased transit light curve and BLS model U CARINAE 1053-59"
set xlabel "Phase"
set ylabel "Magnitude"

plot "stdin.bls.model" u ($5> 0.5 ? $5 - 1.:$5):2, \
"stdin.bls.phcurve" u ($1> 0.5 ? $1 - 1.:$1):2 #w l lt rgb "red" 

