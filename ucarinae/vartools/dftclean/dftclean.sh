awk '{print $1" "$2" "$3}' ../../lc/data/carinae_optical_clean.dat | \
	vartools -i - -oneline -ascii -parallel 4\
    -dftclean 4 maxfreq 10. outdspec salida \
        finddirtypeaks 5 clip 5. 1 \
        outwfunc salida \
        clean 0.5 5.0 outcbeam salida \
        outcspec salida \
        findcleanpeaks 5 clip 5. 1 \
        verboseout > dftclean
