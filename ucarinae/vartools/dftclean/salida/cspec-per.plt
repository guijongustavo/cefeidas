# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key
set title "Periodogram after application of clean deconvolution \n Optical U CARINAE 1053-59"
set output "per-cspec-optical-ucarinae.png"
set xlabel "Period [d]"
set ylabel "Power"
set xrange[1:100]

plot "stdin.dftclean.cspec" u (1/$1):2 w l
