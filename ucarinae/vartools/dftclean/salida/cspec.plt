# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
unset key
set title "DFT Power Spectrum after application of clean deconvolution \n Optical U CARINAE 1053-59"
set output "dft-cspec-optical-ucarinae.png"
set xlabel "Frequency [c/d]"
set ylabel "Power"


plot "stdin.dftclean.cspec" u 1:2 w l
