# Made by:
# Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
# Instituto de Astronomia UNAM
# Ciudad Universitaria
# Ciudad de Mexico
# Mexico

set terminal pngcairo size 640,480
set key autotitle columnhead
unset key
set output "./images/carinae_optical_lc-normal.png"
set title "AAVSO/OPTICAL U CARINAE 1053-59"
set xlabel "MJD"
set ylabel "MAGNITUDE"
#set size 1,0.5
plot 'data/carinae_optical_clean.dat' using 1:2:3 title "Magnitude" with yerrorbars lc "blue"

